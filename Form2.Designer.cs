﻿namespace Software_Engineering_Final
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomBooking = new System.Windows.Forms.Button();
            this.roomPPD = new System.Windows.Forms.Label();
            this.roomCapacity = new System.Windows.Forms.Label();
            this.roomCode = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.RoomAdding = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bookedTill = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.price = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // roomBooking
            // 
            this.roomBooking.Location = new System.Drawing.Point(727, 384);
            this.roomBooking.Name = "roomBooking";
            this.roomBooking.Size = new System.Drawing.Size(170, 32);
            this.roomBooking.TabIndex = 29;
            this.roomBooking.Text = "Vacate Room";
            this.roomBooking.UseVisualStyleBackColor = true;
            this.roomBooking.Click += new System.EventHandler(this.roomBooking_Click);
            // 
            // roomPPD
            // 
            this.roomPPD.AutoSize = true;
            this.roomPPD.Location = new System.Drawing.Point(166, 184);
            this.roomPPD.Name = "roomPPD";
            this.roomPPD.Size = new System.Drawing.Size(0, 17);
            this.roomPPD.TabIndex = 28;
            // 
            // roomCapacity
            // 
            this.roomCapacity.AutoSize = true;
            this.roomCapacity.Location = new System.Drawing.Point(166, 138);
            this.roomCapacity.Name = "roomCapacity";
            this.roomCapacity.Size = new System.Drawing.Size(0, 17);
            this.roomCapacity.TabIndex = 27;
            // 
            // roomCode
            // 
            this.roomCode.AutoSize = true;
            this.roomCode.Location = new System.Drawing.Point(166, 93);
            this.roomCode.Name = "roomCode";
            this.roomCode.Size = new System.Drawing.Size(0, 17);
            this.roomCode.TabIndex = 26;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(727, 422);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(170, 32);
            this.button2.TabIndex = 25;
            this.button2.Text = "To Available Rooms";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RoomAdding
            // 
            this.RoomAdding.Location = new System.Drawing.Point(727, 460);
            this.RoomAdding.Name = "RoomAdding";
            this.RoomAdding.Size = new System.Drawing.Size(170, 32);
            this.RoomAdding.TabIndex = 24;
            this.RoomAdding.Text = "To Room Adding";
            this.RoomAdding.UseVisualStyleBackColor = true;
            this.RoomAdding.Click += new System.EventHandler(this.RoomAdding_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 23;
            this.label3.Text = "Price Per Day:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Capacity:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Room Code:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(572, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(325, 24);
            this.comboBox1.TabIndex = 20;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 17);
            this.label5.TabIndex = 32;
            this.label5.Text = "Booked Till";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // bookedTill
            // 
            this.bookedTill.AutoSize = true;
            this.bookedTill.Location = new System.Drawing.Point(169, 227);
            this.bookedTill.Name = "bookedTill";
            this.bookedTill.Size = new System.Drawing.Size(0, 17);
            this.bookedTill.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(350, 436);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 17);
            this.label6.TabIndex = 34;
            this.label6.Text = "Price";
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.Location = new System.Drawing.Point(485, 436);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(0, 17);
            this.price.TabIndex = 35;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 501);
            this.Controls.Add(this.price);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bookedTill);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.roomBooking);
            this.Controls.Add(this.roomPPD);
            this.Controls.Add(this.roomCapacity);
            this.Controls.Add(this.roomCode);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RoomAdding);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form2";
            this.Text = "Taken Rooms";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button roomBooking;
        private System.Windows.Forms.Label roomPPD;
        private System.Windows.Forms.Label roomCapacity;
        private System.Windows.Forms.Label roomCode;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button RoomAdding;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label bookedTill;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label price;
    }
}