﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Software_Engineering_Final
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox1.DisplayMember = "code";
            comboBox1.DataSource = Hotel.availableRooms;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem == null) {return;}
            roomCode.Text = (comboBox1.SelectedItem as Room).getCode();
            roomCapacity.Text = Convert.ToString((comboBox1.SelectedItem as Room).getCapacity());
            roomPPD.Text = Convert.ToString((comboBox1.SelectedItem as Room).getPricePerDay());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 f3 = new Form3();
            f3.ShowDialog();
            this.Close();
        }

        private void roomBooking_Click(object sender, EventArgs e)
        {
            (comboBox1.SelectedItem as Room).bookRoom(Decimal.ToInt32(daysBooked.Value));
            comboBox1.DataSource = null;
            comboBox1.DataSource = Hotel.availableRooms;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void daysBooked_ValueChanged(object sender, EventArgs e)
        {

        }

        private void roomPPD_Click(object sender, EventArgs e)
        {

        }

        private void roomCapacity_Click(object sender, EventArgs e)
        {

        }

        private void roomCode_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }

    public static class Hotel
    {
        public static ArrayList availableRooms = new ArrayList();
        public static ArrayList takenRooms = new ArrayList();
        public static void addRoom(String code, int capacity, bool taken, int pricePerDay)
        {
            Room r = new Room(code, capacity, taken, pricePerDay);
            if (r.getTaken())
            {
                takenRooms.Add(r);
            }
            else
            {
                availableRooms.Add(r);
            }
        }
    }

    public class Room
    {
        public static int numberOfRooms = 0;
        public String code { get; set; }
        protected int capacity;
        protected bool taken;
        protected int pricePerDay;
        protected DateTime bookedTill;

        public Room(String code, int capacity, bool taken, int pricePerDay)
        {
            this.code = code;
            this.capacity = capacity;
            this.taken = taken;
            this.pricePerDay = pricePerDay;
            numberOfRooms++;
        }
        public String getCode()
        {
            return this.code;
        }
        public int getCapacity()
        {
            return this.capacity;
        }
        public bool getTaken()
        {
            return this.taken;
        }
        public int getPricePerDay()
        {
            return this.pricePerDay;
        }
        public DateTime getBookedTill()
        {
            return this.bookedTill;
        }
        public void setCode(String code)
        {
            this.code = code;
        }
        public void setCapacity(int capacity)
        {
            this.capacity = capacity;
        }
        public void setTaken(bool taken)
        {
            this.taken = taken;
        }
        public void setPricePerDay(int pricePerDay)
        {
            this.pricePerDay = pricePerDay;
        }
        public void bookRoom(int days)
        {
            if (this.taken)
            {
                Console.WriteLine("Room already taken!");
            }
            else
            {
                this.taken = true;
                this.bookedTill = DateTime.Now;
                this.bookedTill.AddDays(days);
                Hotel.availableRooms.Remove(this);
                Hotel.takenRooms.Add(this);
                Console.WriteLine("Room " + this.code + " booked");
            }
        }

        public void vacateRoom()
        {
            if (this.taken)
            {
                this.taken = false;
                this.bookedTill = DateTime.Now;
                Hotel.takenRooms.Remove(this);
                Hotel.availableRooms.Add(this);
                Console.WriteLine("Room " + this.code + " vacated");
            }
            else
            {
                Console.WriteLine("This room is already vacant!");
            }
        }
    }
}
