﻿namespace Software_Engineering_Final
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.RoomAdding = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.roomCode = new System.Windows.Forms.Label();
            this.roomCapacity = new System.Windows.Forms.Label();
            this.roomPPD = new System.Windows.Forms.Label();
            this.roomBooking = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.daysBooked = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.daysBooked)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(554, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(325, 24);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Room Code:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Capacity:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Price Per Day:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // RoomAdding
            // 
            this.RoomAdding.Location = new System.Drawing.Point(709, 443);
            this.RoomAdding.Name = "RoomAdding";
            this.RoomAdding.Size = new System.Drawing.Size(170, 32);
            this.RoomAdding.TabIndex = 12;
            this.RoomAdding.Text = "To Room Adding";
            this.RoomAdding.UseVisualStyleBackColor = true;
            this.RoomAdding.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(709, 405);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(170, 32);
            this.button2.TabIndex = 13;
            this.button2.Text = "To Taken Rooms";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // roomCode
            // 
            this.roomCode.AutoSize = true;
            this.roomCode.Location = new System.Drawing.Point(148, 76);
            this.roomCode.Name = "roomCode";
            this.roomCode.Size = new System.Drawing.Size(0, 17);
            this.roomCode.TabIndex = 14;
            this.roomCode.Click += new System.EventHandler(this.roomCode_Click);
            // 
            // roomCapacity
            // 
            this.roomCapacity.AutoSize = true;
            this.roomCapacity.Location = new System.Drawing.Point(148, 121);
            this.roomCapacity.Name = "roomCapacity";
            this.roomCapacity.Size = new System.Drawing.Size(0, 17);
            this.roomCapacity.TabIndex = 15;
            this.roomCapacity.Click += new System.EventHandler(this.roomCapacity_Click);
            // 
            // roomPPD
            // 
            this.roomPPD.AutoSize = true;
            this.roomPPD.Location = new System.Drawing.Point(148, 167);
            this.roomPPD.Name = "roomPPD";
            this.roomPPD.Size = new System.Drawing.Size(0, 17);
            this.roomPPD.TabIndex = 16;
            this.roomPPD.Click += new System.EventHandler(this.roomPPD_Click);
            // 
            // roomBooking
            // 
            this.roomBooking.Location = new System.Drawing.Point(709, 367);
            this.roomBooking.Name = "roomBooking";
            this.roomBooking.Size = new System.Drawing.Size(170, 32);
            this.roomBooking.TabIndex = 17;
            this.roomBooking.Text = "Book Room";
            this.roomBooking.UseVisualStyleBackColor = true;
            this.roomBooking.Click += new System.EventHandler(this.roomBooking_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Days to book";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // daysBooked
            // 
            this.daysBooked.Location = new System.Drawing.Point(151, 209);
            this.daysBooked.Name = "daysBooked";
            this.daysBooked.Size = new System.Drawing.Size(120, 22);
            this.daysBooked.TabIndex = 19;
            this.daysBooked.ValueChanged += new System.EventHandler(this.daysBooked_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 513);
            this.Controls.Add(this.daysBooked);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.roomBooking);
            this.Controls.Add(this.roomPPD);
            this.Controls.Add(this.roomCapacity);
            this.Controls.Add(this.roomCode);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RoomAdding);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "Available Rooms";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.daysBooked)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button RoomAdding;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label roomCode;
        private System.Windows.Forms.Label roomCapacity;
        private System.Windows.Forms.Label roomPPD;
        private System.Windows.Forms.Button roomBooking;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown daysBooked;
    }
}

