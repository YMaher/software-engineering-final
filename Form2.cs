﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Software_Engineering_Final
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            comboBox1.DisplayMember = "code";
            comboBox1.DataSource = Hotel.takenRooms;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null) { return; }
            roomCode.Text = (comboBox1.SelectedItem as Room).getCode();
            roomCapacity.Text = Convert.ToString((comboBox1.SelectedItem as Room).getCapacity());
            roomPPD.Text = Convert.ToString((comboBox1.SelectedItem as Room).getPricePerDay());
            bookedTill.Text = Convert.ToString((comboBox1.SelectedItem as Room).getBookedTill());
        }

        private void RoomAdding_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 f3 = new Form3();
            f3.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.ShowDialog();
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void roomBooking_Click(object sender, EventArgs e)
        {
            price.Text = Convert.ToString((comboBox1.SelectedItem as Room).getPricePerDay() * ((comboBox1.SelectedItem as Room).getBookedTill().Day-DateTime.Now.Day));
            (comboBox1.SelectedItem as Room).vacateRoom();
            comboBox1.DataSource = null;
            comboBox1.DataSource = Hotel.availableRooms;
        }
    }
}
