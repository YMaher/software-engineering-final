﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Software_Engineering_Final
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void addRoomBTN_Click(object sender, EventArgs e)
        {
            Hotel.addRoom(textBox1.Text, Decimal.ToInt32(numericUpDown2.Value), checkBox1.Checked, Decimal.ToInt32(numericUpDown1.Value));
            this.Hide();
            Form1 f1 = new Form1();
            f1.ShowDialog();
            this.Close();
        }
    }
}
